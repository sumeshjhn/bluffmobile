package com.ece452.bluffmobile.customviews;

import android.app.Activity;
import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.ece452.bluffmobile.BluffConst;
import com.ece452.bluffmobile.R;
import com.ece452.bluffmobile.models.BluffCardModel.Rank;
import com.ece452.bluffmobile.models.BluffCardModel.Suit;

public class BluffCardView extends RelativeLayout {

	private Rank mRank = null;
	private Suit mSuit = null;

	public BluffCardView(Context context) {
		super(context);
		initCardView(null);
	}

	public BluffCardView(Context context, AttributeSet attrs) {
		super(context, attrs);
		initCardView(attrs);
	}

	public BluffCardView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		initCardView(attrs);
	}

	private void initCardView(AttributeSet attrs) {
		((Activity) getContext()).getLayoutInflater().inflate(R.layout.card,
				this, true);
		if (attrs != null) {
			int[] stylable = R.styleable.BluffCard;
			TypedArray a = getContext().obtainStyledAttributes(attrs, stylable,
					0, 0);
			// when recycling, may need to set some styles
			// setColor(a.getInt(R.styleable.BluffCard_bc_color, 0xF00F00));

			a.recycle();
		}
	}

	public void setBluffCardRank(Rank rank) {
		TextView txtRank = (TextView) this.findViewById(R.id.txtRank);
		txtRank.setText(rank.getRankInt() + "");
		this.mRank = rank;
		if (this.mSuit != null) {
			this.setCardImage();
		}
	}

	public void setBluffCardSuit(Suit suit) {
		TextView txtSuit = (TextView) this.findViewById(R.id.txtSuit);
		txtSuit.setText(suit.getSuitInt() + "");
		this.mSuit = suit;
		if (this.mRank != null) {
			this.setCardImage();
		}
	}

	private void setCardImage() {
		ImageView _cardImage = (ImageView) this.findViewById(R.id.cardImage);
		int cardImageId = 0;
		
		if (mRank == Rank.EMPTY || mSuit == Suit.EMPTY) {
			cardImageId = R.drawable.b1fv; 
		} else {
			int cardNumber = (mRank.getRankInt() - Rank.TWO.getRankInt()) * BluffConst.NUM_SUITS + 
					mSuit.getSuitInt() - Suit.DIAMONDS.getSuitInt();
			switch(cardNumber) {
			case 0: { cardImageId = R.drawable.c0; break; }
			case 1: { cardImageId = R.drawable.c1; break; }
			case 2: { cardImageId = R.drawable.c2; break; }
			case 3: { cardImageId = R.drawable.c3; break; }
			case 4: { cardImageId = R.drawable.c4; break; }
			case 5: { cardImageId = R.drawable.c5; break; }
			case 6: { cardImageId = R.drawable.c6; break; }
			case 7: { cardImageId = R.drawable.c7; break; }
			case 8: { cardImageId = R.drawable.c8; break; }
			case 9: { cardImageId = R.drawable.c9; break; }
			case 10: { cardImageId = R.drawable.c10; break; }
			case 11: { cardImageId = R.drawable.c11; break; }
			case 12: { cardImageId = R.drawable.c12; break; }
			case 13: { cardImageId = R.drawable.c13; break; }
			case 14: { cardImageId = R.drawable.c14; break; }
			case 15: { cardImageId = R.drawable.c15; break; }
			case 16: { cardImageId = R.drawable.c16; break; }
			case 17: { cardImageId = R.drawable.c17; break; }
			case 18: { cardImageId = R.drawable.c18; break; }
			case 19: { cardImageId = R.drawable.c19; break; }
			case 20: { cardImageId = R.drawable.c20; break; }
			case 21: { cardImageId = R.drawable.c21; break; }
			case 22: { cardImageId = R.drawable.c22; break; }
			case 23: { cardImageId = R.drawable.c23; break; }
			case 24: { cardImageId = R.drawable.c24; break; }
			case 25: { cardImageId = R.drawable.c25; break; }
			case 26: { cardImageId = R.drawable.c26; break; }
			case 27: { cardImageId = R.drawable.c27; break; }
			case 28: { cardImageId = R.drawable.c28; break; }
			case 29: { cardImageId = R.drawable.c29; break; }
			case 30: { cardImageId = R.drawable.c30; break; }
			case 31: { cardImageId = R.drawable.c31; break; }
			case 32: { cardImageId = R.drawable.c32; break; }
			case 33: { cardImageId = R.drawable.c33; break; }
			case 34: { cardImageId = R.drawable.c34; break; }
			case 35: { cardImageId = R.drawable.c35; break; }
			case 36: { cardImageId = R.drawable.c36; break; }
			case 37: { cardImageId = R.drawable.c37; break; }
			case 38: { cardImageId = R.drawable.c38; break; }
			case 39: { cardImageId = R.drawable.c39; break; }
			case 40: { cardImageId = R.drawable.c40; break; }
			case 41: { cardImageId = R.drawable.c41; break; }
			case 42: { cardImageId = R.drawable.c42; break; }
			case 43: { cardImageId = R.drawable.c43; break; }
			case 44: { cardImageId = R.drawable.c44; break; }
			case 45: { cardImageId = R.drawable.c45; break; }
			case 46: { cardImageId = R.drawable.c46; break; }
			case 47: { cardImageId = R.drawable.c47; break; }
			case 48: { cardImageId = R.drawable.c48; break; }
			case 49: { cardImageId = R.drawable.c49; break; }
			case 50: { cardImageId = R.drawable.c50; break; }
			case 51: { cardImageId = R.drawable.c51; break; }
			default: { cardImageId = R.drawable.b1fv; break; }
			}
		}

		_cardImage.setImageDrawable(this.getResources()
				.getDrawable(cardImageId));
	}

}
