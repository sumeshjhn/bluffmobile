package com.ece452.bluffmobile.customviews;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.ece452.bluffmobile.R;
import com.ece452.bluffmobile.models.BluffHandModel;
import com.ece452.bluffmobile.models.BluffPlayerModel;

public class BluffTableView extends RelativeLayout {

	private LinearLayout mHandClaimLayout = null;

	public BluffTableView(Context context) {
		super(context);
		initBluffTable(null);
	}

	public BluffTableView(Context context, AttributeSet attrs) {
		super(context, attrs);
		initBluffTable(attrs);
	}

	public BluffTableView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		initBluffTable(attrs);
	}

	private void initBluffTable(AttributeSet attrs) {
		((Activity) getContext()).getLayoutInflater().inflate(
				R.layout.bluff_table, this, true);

		mHandClaimLayout = (LinearLayout) findViewById(R.id.handClaimLayout);

		if (attrs != null) {
			int[] styleable = R.styleable.BluffTable;
			TypedArray a = getContext().obtainStyledAttributes(attrs,
					styleable, 0, 0);

			// may need to reset styles for recycling
			// setColor(a.getInt(R.styleable.BluffTable_bt_color, 0xF00F00));
			a.recycle();
		}
	}

	public void displayCallOutcome(BluffPlayerModel calledPlayer,
			String callingPlayer, Boolean isHandPresent) {

		String _calledPlayerHand = calledPlayer
				.getHand()
				.getHandTextRepresentation(
						getResources().getStringArray(R.array.handClaimStrings),
						getResources().getString(R.string.no_hand_claimed_text));
		String _displayMessage = "";

		if (!isHandPresent) {
			_displayMessage = getResources().getString(
					R.string.callingplayer_won_call_text);
		} else {
			_displayMessage = getResources().getString(
					R.string.callingplayer_lost_call_text);
		}

		Object[] _arguments = { callingPlayer, calledPlayer.getPlayerName(),
				_calledPlayerHand };
		MessageFormat _displayMessageFormat = new MessageFormat(_displayMessage);

		Toast toast = Toast.makeText(getContext(),
				_displayMessageFormat.format(_arguments), Toast.LENGTH_SHORT);
		toast.show();
	}

	public void placeHandClaimOnTable(BluffHandModel bHM) {
		TextView _handClaimTextView = (TextView) this.mHandClaimLayout
				.findViewById(R.id.handClaimStringRepresentation);
		_handClaimTextView.setText(bHM.getHandTextRepresentation(getResources()
				.getStringArray(R.array.handClaimStrings), getResources()
				.getString(R.string.no_hand_claimed_text)));

	}

	/*public void enableCall(boolean enabled) {
		((Button) this.findViewById(R.id.btnCall)).setEnabled(enabled);
	}*/
}
