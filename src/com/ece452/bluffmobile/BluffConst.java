package com.ece452.bluffmobile;

public class BluffConst {

	// values
	public static final int DEFAULTTIMEOUT = 20000; // in ms
	public static final int INITIAL_NUM_OF_CARDS = 5;
	public static final int RANKS_IN_SUIT = 13;
	public static final int NUM_SUITS = 4;

	// server url
	public final static String WSURI = "ws://50.57.108.57:9000";// "ws://192.168.0.112:9000";

	// JSON REQUEST KEYS
	public final static Integer JSON_REQ_ACT_LOGIN = Integer.valueOf(1);
	public static final Integer JSON_REQ_ACT_BLUFF = Integer.valueOf(2);
	public static final Integer JSON_REQ_ACT_CALL = Integer.valueOf(3);
	public static final Integer JSON_REQ_ACT_STARTGAME = Integer.valueOf(5);

	// JSON REPSONSE KEYS
	public final static Integer JSON_RESP_ACT_STARTROUND = Integer.valueOf(99);
	public static final Integer JSON_RESP_ACT_BLUFF = Integer.valueOf(98);
	public static final Integer JSON_RESP_ACT_CALL = Integer.valueOf(97);
	public static final Integer JSON_RESP_ACT_ENDGAME = Integer.valueOf(96);

	// JSON RESPONSE VALUES
	public final static Integer JSON_VAL_SUCCESS = Integer.valueOf(1);
	public final static Integer JSON_VAL_FAIL = Integer.valueOf(0);

	public final static String JSON_KEY_CARDS = "cards";
	public final static String JSON_KEY_NUMCARDS = "cards";
	public final static String JSON_KEY_NAME = "name";
	public final static String JSON_KEY_HAND = "hand";
	public final static String JSON_KEY_HANDTYPE = "handType";
	public final static String JSON_KEY_ACTION = "action";
	public final static String JSON_KEY_RESP = "response";
	public static final String JSON_KEY_PLAYER = "player";
	public static final String JSON_KEY_PLAYERS = "players";
	public static final String JSON_KEY_USERTURN = "isUserTurn";
	public static final String JSON_KEY_RANK = "rank";
	public static final String JSON_KEY_RANKS = "ranks";
	public static final String JSON_KEY_SUIT = "suit";
	public final static String JSON_KEY_GAMEID = "gameID";
	public static final String JSON_KEY_HANDOWNER = "handOwner";
	public static final String JSON_KEY_CALLINGPLAYER = "callingPlayer";
	public static final String JSON_KEY_CALLEDPLAYER = "calledPlayer";
	public static final String JSON_KEY_ISHANDPRESENT = "isHandPresent";
	public static final String JSON_KEY_NUMCARDSLEFT = "cardsLeft";
	public static final String JSON_KEY_WINNER = "winner";

	/**
	 * Activities putExtra message keys
	 */
	public static final String KEY_PLAYERNAME = "PLAYER_NAME";
	public static final String KEY_SETUPDETAILS = "SETUP_DETAILS";

}
