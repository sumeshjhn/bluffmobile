package com.ece452.bluffmobile.customcontrols;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.DialogFragment;
import android.app.FragmentManager;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;

import com.ece452.bluffmobile.BluffConst;
import com.ece452.bluffmobile.R;
import com.ece452.bluffmobile.models.BluffCardModel;
import com.ece452.bluffmobile.models.BluffHandModel;
import com.ece452.bluffmobile.models.BluffHandModel.HandType;
import com.ece452.bluffmobile.network.Network;

public class UserTurnDialogFragment extends DialogFragment {

	private enum SpinnerCardTags {
		HANDTYPE(0), FIRST(1), SECOND(2), THIRD(3), FOURTH(4);

		private final int mId;

		private SpinnerCardTags(int id) {
			this.mId = id;
		}

		public int getSpinnerCardTagsInt() {
			return mId;
		}
	}

	private BluffHandModel mMinimumHand = null;

	private String mHandOwner = null;

	private final String mDialogFragmentTag = "dialog";

	private BluffHandModel mHand = null;

	private List<Spinner> mRankSpinners = null;
	
	private HandType mPokerHandType = null;

	private String[] mDefaultArrayOfCardValues = null;
	
	private boolean mFirstTurn = true;

	// private int mPrimRank = 0;
	// private int mSecRank = 0;
	// private int mTertRank = 0;
	// private int mQuatRank = 0;
	//
	// private int mSuit = 0;
	//
	// private int mHandType = 0;

	View mUserTurnDialog = null;

	// private static UserTurnDialogFragment mUTDF = new
	// UserTurnDialogFragment();
	//
	// public static UserTurnDialogFragment getInstance() {
	// if (mUTDF == null) {
	// mUTDF = new UserTurnDialogFragment();
	// }
	// mUTDF.setCancelable(false);
	// return mUTDF;
	// }

	public UserTurnDialogFragment() {
		this.setCancelable(false);
		this.mHand = new BluffHandModel();
		this.mRankSpinners = new ArrayList<Spinner>();
		this.mFirstTurn=true;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		mUserTurnDialog = inflater.inflate(R.layout.dialog_select_hand,
				container, false);

		this.mDefaultArrayOfCardValues = getResources().getStringArray(
				R.array.cardValues);
		
		if (this.mFirstTurn) {
			this.enableCall(false);
			this.mFirstTurn=false;
		}

		this.mRankSpinners = new ArrayList<Spinner>();
		final Spinner firstHighestNumberSpinner = loadBluffSpinner(
				R.id.firstHighestNumber, R.array.cardValues);
		this.mRankSpinners.add(firstHighestNumberSpinner);
		firstHighestNumberSpinner.setTag(SpinnerCardTags.FIRST);

		final Spinner secondHighestNumberSpinner = loadBluffSpinner(
				R.id.secondHighestNumber, R.array.cardValues);
		this.mRankSpinners.add(secondHighestNumberSpinner);
		secondHighestNumberSpinner.setTag(SpinnerCardTags.SECOND);

		final Spinner thirdHighestNumberSpinner = loadBluffSpinner(
				R.id.thirdHighestNumber, R.array.cardValues);
		this.mRankSpinners.add(thirdHighestNumberSpinner);
		thirdHighestNumberSpinner.setTag(SpinnerCardTags.THIRD);

		final Spinner fourthHighestNumberSpinner = loadBluffSpinner(
				R.id.fourthHighestNumber, R.array.cardValues);
		this.mRankSpinners.add(fourthHighestNumberSpinner);
		fourthHighestNumberSpinner.setTag(SpinnerCardTags.FOURTH);
		
		OnItemSelectedListener _rankSelectedListener = new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> arg0, View arg1,
					int arg2, long arg3) {
				removeRanksFromSpinners(arg0);
			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
				// TODO Auto-generated method stub

			}
		};

		firstHighestNumberSpinner
				.setOnItemSelectedListener(_rankSelectedListener);
		secondHighestNumberSpinner
				.setOnItemSelectedListener(_rankSelectedListener);
		thirdHighestNumberSpinner
				.setOnItemSelectedListener(_rankSelectedListener);
		
		fourthHighestNumberSpinner.setOnItemSelectedListener(new OnItemSelectedListener() {
			public void onItemSelected(android.widget.AdapterView<?> arg0, View arg1, int arg2, long arg3) {
				if (arg0.getSelectedItem() != null) {
					((Button) mUserTurnDialog.findViewById(R.id.btnDialogSubmit)).setEnabled(true);
				} else {
					((Button) mUserTurnDialog.findViewById(R.id.btnDialogSubmit)).setEnabled(false);
				}
			};
			
			public void onNothingSelected(android.widget.AdapterView<?> arg0) {
				
			};
		});

		final Spinner pokerSuitSpinner = loadBluffSpinner(R.id.pokerSuit,
				R.array.cardSuits);
		mHand.setHandSuit(BluffCardModel.Suit.EMPTY);

		final Spinner pokerHandSpinner = loadBluffSpinner(R.id.pokerHand,
				R.array.pokerHands);

		pokerHandSpinner
				.setOnItemSelectedListener(new OnItemSelectedListener() {

					@Override
					public void onItemSelected(AdapterView<?> arg0, View arg1,
							int arg2, long arg3) {
						firstHighestNumberSpinner.setVisibility(View.VISIBLE);
						secondHighestNumberSpinner.setVisibility(View.GONE);
						thirdHighestNumberSpinner.setVisibility(View.GONE);
						fourthHighestNumberSpinner.setVisibility(View.GONE);
						pokerSuitSpinner.setVisibility(View.GONE);

						Object adapterValue = arg0.getAdapter().getItem(arg2);
						if (adapterValue.toString().equals("TWOPAIR")
								|| adapterValue.toString().equals("FULLHOUSE")
								|| adapterValue.toString().equals("FLUSH")) {
							secondHighestNumberSpinner
									.setVisibility(View.VISIBLE);
						}

						if (adapterValue.toString().equals("FLUSH")
								|| adapterValue.toString().equals(
										"STRAIGHT FLUSH")) {
							pokerSuitSpinner.setVisibility(View.VISIBLE);
							if (adapterValue.toString().equals("FLUSH")) {
								thirdHighestNumberSpinner
										.setVisibility(View.VISIBLE);
								fourthHighestNumberSpinner
										.setVisibility(View.VISIBLE);
							}
						}
						removeRanksFromSpinners(arg0);
					}

					@Override
					public void onNothingSelected(AdapterView<?> arg0) {
						mHand.setHandType(BluffHandModel.HandType.EMPTY);
					}
				});
		pokerHandSpinner.setSelection(0);

		OnClickListener bluffButtonListener = new OnClickListener() {

			@Override
			public void onClick(View v) {
				// submit information to server and dismiss dialog
				JSONObject object = new JSONObject();
				JSONObject handObject = new JSONObject();
				JSONArray handObjectRanks = new JSONArray();

				try {
					object.put(BluffConst.JSON_KEY_ACTION,
							BluffConst.JSON_REQ_ACT_BLUFF);

					mHand.setHandType(BluffHandModel
							.convertToHandTypeEnum(pokerHandSpinner
									.getSelectedItem().toString()));

					handObject.put(BluffConst.JSON_KEY_HANDTYPE, mHand
							.getHandType().getHandInt());

					if (pokerSuitSpinner.getVisibility() == View.VISIBLE) {
						mHand.setHandSuit(BluffCardModel
								.convertToCardSuitEnum(pokerSuitSpinner
										.getSelectedItem().toString()));
					}

					handObject.put(BluffConst.JSON_KEY_SUIT, mHand
							.getHandSuit().getSuitInt());

					// this should be changed, I don't like where/how this is
					// being done

					mHand.getCardRanks().clear();

					if (firstHighestNumberSpinner.getVisibility() == View.VISIBLE) {
						mHand.addCardRank(BluffCardModel
								.convertToCardRankEnum(firstHighestNumberSpinner
										.getSelectedItem().toString()));
					}
					if (secondHighestNumberSpinner.getVisibility() == View.VISIBLE) {
						mHand.addCardRank(BluffCardModel
								.convertToCardRankEnum(secondHighestNumberSpinner
										.getSelectedItem().toString()));
					}
					if (thirdHighestNumberSpinner.getVisibility() == View.VISIBLE) {
						mHand.addCardRank(BluffCardModel
								.convertToCardRankEnum(thirdHighestNumberSpinner
										.getSelectedItem().toString()));
					}
					if (fourthHighestNumberSpinner.getVisibility() == View.VISIBLE) {
						mHand.addCardRank(BluffCardModel
								.convertToCardRankEnum(fourthHighestNumberSpinner
										.getSelectedItem().toString()));
					}

					for (int i = 0; i < mHand.getCardRanks().size(); i++) {
						handObjectRanks.put(i, mHand.getCardRanks().get(i)
								.getRankInt());
					}

					handObject.put(BluffConst.JSON_KEY_RANKS, handObjectRanks);
					object.put(BluffConst.JSON_KEY_HAND, handObject);
					Log.d("json send", object.toString());
					Network.getInstance().sendMessage(object);
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				dismiss();
			}
		};

		OnClickListener callButtonListener = new OnClickListener() {

			@Override
			public void onClick(View v) {
				// submit information to server and dismiss dialog
				JSONObject object = new JSONObject();

				try {
					object.put(BluffConst.JSON_KEY_ACTION,
							BluffConst.JSON_REQ_ACT_CALL);

					object.put(BluffConst.JSON_KEY_PLAYER, mHandOwner);
					Log.d("json send", object.toString());
					Network.getInstance().sendMessage(object);
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				dismiss();
			}
		};
		((Button) mUserTurnDialog.findViewById(R.id.btnDialogSubmit))
				.setOnClickListener(bluffButtonListener);
		((Button) mUserTurnDialog.findViewById(R.id.btnDialogCall))
				.setOnClickListener(callButtonListener);

		return mUserTurnDialog;

	}
	
	public void removeRanksFromSpinners(AdapterView<?> adapterView) {

		// firstHighest must be larger than mMinimumHand's rank

		// must tell secondHighest spinner to be lower than
		// firstHigher
		int start = 0;

		switch ((SpinnerCardTags) adapterView.getTag()) {
		case HANDTYPE:
			if (adapterView.getAdapter().getCount() > 0) {
				mPokerHandType = BluffHandModel.convertToHandTypeEnum(adapterView.getSelectedItem().toString());
				adapterView.setEnabled(true);
			} else {
				adapterView.setEnabled(false);
			}
			start = 0;
			break;
		case FIRST:
			start = 1;
			break;
		case SECOND:
			start = 2;
			break;
		case THIRD:
			start = 3;
			break;
		case FOURTH:
			break;
		default:
			break;
		}
		
		boolean enforcedMinimum = true;
		if (mMinimumHand == null || mMinimumHand.getHandType().getHandInt() < mPokerHandType.getHandInt()) {
			enforcedMinimum = false;
		} else {
			for (int i = 0; i < start; i++) {
				if (BluffCardModel.convertToCardRankEnum(mRankSpinners.get(i).getSelectedItem().toString()).getRankInt() > 
						mMinimumHand.getCardRanks().get(i).getRankInt()) {
					enforcedMinimum = false;
				}
			}
		}

		BluffCardModel.Rank _previousMinimumItem = start == 0 ? BluffCardModel.Rank.EMPTY :
				BluffCardModel.convertToCardRankEnum(adapterView.getSelectedItem()
						.toString());
		
		if (adapterView.getSelectedItem() != null && mRankSpinners.get(start).getVisibility() != View.VISIBLE) {
			((Button) mUserTurnDialog.findViewById(R.id.btnDialogSubmit)).setEnabled(true);
		} else {
			((Button) mUserTurnDialog.findViewById(R.id.btnDialogSubmit)).setEnabled(false);
		}
		
		mRankSpinners.get(start).setEnabled(true);
		for (int i = start + 1; i < mRankSpinners.size(); i++) {
			mRankSpinners.get(i).setEnabled(false);
		}

		for (int i = start; i < mRankSpinners.size(); i++) {

			List<CharSequence> _listOfCards = new ArrayList<CharSequence>();

			for (int j = 0; j < mDefaultArrayOfCardValues.length; j++) {
				BluffCardModel.Rank _itemValue = BluffCardModel
						.convertToCardRankEnum(mDefaultArrayOfCardValues[j]);
				if ((_itemValue.getRankInt() < _previousMinimumItem.getRankInt() 
						|| _previousMinimumItem == BluffCardModel.Rank.EMPTY)
						&& (!enforcedMinimum || i < mMinimumHand.getCardRanks().size()
								&& _itemValue.getRankInt() >= mMinimumHand.getCardRanks().get(i).getRankInt())
								&& (mPokerHandType != BluffHandModel.HandType.STRAIGHT && mPokerHandType != BluffHandModel.HandType.STRAIGHTFLUSH
								|| _itemValue.getRankInt() >= BluffCardModel.Rank.SIX.getRankInt())) {
					if (enforcedMinimum && i < mMinimumHand.getCardRanks().size()) {
						if (_itemValue.getRankInt() > mMinimumHand.getCardRanks().get(i).getRankInt())
							enforcedMinimum = false;
						else if (i == mMinimumHand.getCardRanks().size() - 1)
							continue;
					}
					_listOfCards.add(mDefaultArrayOfCardValues[j]);
				}
			}

			ArrayAdapter<CharSequence> _newArrayAdapter = new ArrayAdapter<CharSequence>(
					getActivity(),
					android.R.layout.simple_spinner_item, _listOfCards);

			if (_previousMinimumItem != BluffCardModel.Rank.EMPTY && _previousMinimumItem != BluffCardModel.Rank.TWO) {
				_previousMinimumItem = BluffCardModel.Rank.values()[_previousMinimumItem.getRankInt() - 2];
			}
			mRankSpinners.get(i).setAdapter(_newArrayAdapter);
			mRankSpinners.get(i).invalidate();
			if (mRankSpinners.get(i).getAdapter().getCount() == 0) {
				mRankSpinners.get(i).setEnabled(false);
			}
		}
	}

	private boolean isMaxHandofType() {
		if (this.mMinimumHand == null)
			return false;
		for (int i = 0; i < this.mMinimumHand.getCardRanks().size(); i++) {
			if (this.mMinimumHand.getCardRanks().get(i).getRankInt() < BluffCardModel.Rank.ACE.getRankInt() - i) {
				return false;
			}
		}
		return true;
	}
	
	private Spinner loadBluffSpinner(int viewId, int adapterValues) {
		Spinner _bluffSpinner = (Spinner) mUserTurnDialog.findViewById(viewId);
		
		ArrayAdapter<CharSequence> _bluffAdapter = null;

		if (viewId == R.id.pokerHand) {
			String[] _defaultArrayOfHandValues = getResources().getStringArray(R.array.pokerHands);
			
			List<CharSequence> _listOfHands = new ArrayList<CharSequence>();
			boolean addedValue = false;
			for (int i = 0; i < _defaultArrayOfHandValues.length; i++) {
				if (this.mMinimumHand == null || BluffHandModel.convertToHandTypeEnum(
						_defaultArrayOfHandValues[i]).getHandInt() > this.mMinimumHand.getHandType().getHandInt()
						|| BluffHandModel.convertToHandTypeEnum(_defaultArrayOfHandValues[i]).getHandInt() 
						== this.mMinimumHand.getHandType().getHandInt() && !isMaxHandofType()) {
					if (!addedValue) {
						mPokerHandType = BluffHandModel.convertToHandTypeEnum(_defaultArrayOfHandValues[i]);
						addedValue = true;
					}
					_listOfHands.add(_defaultArrayOfHandValues[i]);
				}
			}
			_bluffAdapter = new ArrayAdapter<CharSequence>(this.getActivity(),
					android.R.layout.simple_spinner_item, _listOfHands);
		} else {
			_bluffAdapter = ArrayAdapter.createFromResource(this.getActivity(),
					adapterValues, android.R.layout.simple_spinner_item);
		}

		_bluffAdapter
				.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		_bluffSpinner.setAdapter(_bluffAdapter);
		
		if (viewId == R.id.pokerHand) {
			_bluffSpinner.setTag(SpinnerCardTags.HANDTYPE);
			removeRanksFromSpinners(_bluffSpinner);
		}

		return _bluffSpinner;
	}

	public void showDialog(FragmentManager fragmentManager, boolean firstTurn) {

		/*
		 * // DialogFragment.show() will take care of adding the fragment // in
		 * a transaction. We also want to remove any currently showing //
		 * dialog, so make our own transaction and take care of that here.
		 * FragmentTransaction ft = getFragmentManager().beginTransaction();
		 * Fragment prev = getFragmentManager().findFragmentByTag("dialog"); if
		 * (prev != null) { ft.remove(prev); } ft.addToBackStack(null);
		 * 
		 * // Create and show the dialog. DialogFragment newFragment = new
		 * UserTurnDialogFragment(); newFragment.show(ft, "dialog");
		 */
		this.show(fragmentManager, mDialogFragmentTag);
		this.mFirstTurn=firstTurn;
	}

	public void setMinimumHand(BluffHandModel minimumHand, String handOwner) {
		this.mMinimumHand = minimumHand;
		this.mHandOwner = handOwner;
	}

	public void enableCall(boolean enabled) {
		((Button) mUserTurnDialog.findViewById(R.id.btnDialogCall))
				.setEnabled(enabled);
	}
}
