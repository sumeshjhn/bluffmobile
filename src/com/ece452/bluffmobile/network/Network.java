package com.ece452.bluffmobile.network;

import org.json.JSONException;
import org.json.JSONObject;

import android.util.Log;

import com.ece452.bluffmobile.BluffConst;

import de.tavendo.autobahn.WebSocketConnection;
import de.tavendo.autobahn.WebSocketException;
import de.tavendo.autobahn.WebSocketHandler;

public class Network {

	private final WebSocketConnection mConnection = new WebSocketConnection();
	private static Network instance = null;
	private NetworkCallback listener = null;

	public synchronized static Network getInstance() {
		if (instance == null) {
			instance = new Network();
		}
		return instance;
	}

	private void start() {
		try {
			mConnection.connect(BluffConst.WSURI, new WebSocketHandler() {

				@Override
				public void onOpen() {
					Log.d("Harnek", "Status: Connected to " + BluffConst.WSURI);
					// connected = true;
					connectionUpdate();
				}

				@Override
				public void onTextMessage(String payload) {
					Log.d("Harnek", "Got echo: " + payload);
					JSONObject json = null;
					try {
						json = new JSONObject(payload);
					} catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					if (json == null) {
						// Return a failure response to client
					} else {
						if (listener != null) {
							listener.onMessageReceived(json);
						}
					}
				}

				@Override
				public void onClose(int code, String reason) {
					// connected = false;
					Log.d("Harnek", "Connection lost.=" + reason);
					connectionUpdate();
				}
			});
		} catch (WebSocketException e) {
			// connected = false;
			Log.d("Harnek", e.toString());
			connectionUpdate();
		}

	}

	public void setCallback(NetworkCallback listener) {
		this.listener = listener;
	}

	public void sendMessage(JSONObject object) {

		/*
		 * Sample way of sending messages. JSONObject object = new JSONObject();
		 * try { object.put("name", "Jack Hack"); object.put("score", new
		 * Integer(200)); object.put("current", new Double(152.32));
		 * object.put("nickname", "Hacker"); } catch (JSONException e) {
		 * e.printStackTrace(); }
		 */
		mConnection.sendTextMessage(object.toString());
	}

	public void connect() {
		if (!this.mConnection.isConnected()) {
			start();
		}
	}

	public boolean isConnected() {
		return this.mConnection.isConnected();
	}

	public void disconnect() {
		if (this.isConnected()) {
			mConnection.disconnect();
		}
	}

	private void connectionUpdate() {
		if (listener != null) {
			listener.connectionUpdate(this.mConnection.isConnected());
		}

	}

}
