package com.ece452.bluffmobile.network;

import org.json.JSONObject;

public interface NetworkCallback {

	public void onMessageReceived (JSONObject jsonResponse);
	public void connectionUpdate(boolean connected);
}
