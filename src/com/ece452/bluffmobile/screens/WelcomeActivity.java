package com.ece452.bluffmobile.screens;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;

import com.ece452.bluffmobile.BluffConst;
import com.ece452.bluffmobile.R;
import com.ece452.bluffmobile.network.Network;

public class WelcomeActivity extends Activity {

	private final String Tag = "WelcomeActivity";
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_welcome);

		final Button btnStartGame = (Button) this.findViewById(R.id.btnStart);
		final EditText etxtPlayerName = (EditText) this
				.findViewById(R.id.etxtPlayerName);

		etxtPlayerName.addTextChangedListener(new TextWatcher() {

			@Override
			public void afterTextChanged(Editable arg0) {
				// TODO Auto-generated method stub

			}

			@Override
			public void beforeTextChanged(CharSequence arg0, int arg1,
					int arg2, int arg3) {
				// TODO Auto-generated method stub
			}

			@Override
			public void onTextChanged(CharSequence arg0, int arg1, int arg2,
					int arg3) {
				if (TextUtils.isEmpty(arg0)) {
					btnStartGame.setEnabled(false);
				} else {
					btnStartGame.setEnabled(true);
				}
			}
		});

		btnStartGame.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				Intent pushWaitingScreen = new Intent(getApplicationContext(),
						SetupActivity.class);
				pushWaitingScreen.putExtra(BluffConst.KEY_PLAYERNAME,
						etxtPlayerName.getText().toString());
				startActivity(pushWaitingScreen);
				// finish();

			}
		});
	}

}
