package com.ece452.bluffmobile.screens;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.widget.Toast;

import com.ece452.bluffmobile.BluffConst;
import com.ece452.bluffmobile.R;
import com.ece452.bluffmobile.network.Network;
import com.ece452.bluffmobile.network.NetworkCallback;

public class SetupActivity extends Activity implements NetworkCallback {

	private static final String Tag = "SetupActivity";
	private Handler mHandler = null;
	private Runnable mTimeoutRunnable = null;
	private String name;

	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_waiting_screen);
		Intent intent = this.getIntent();
		name = intent.getStringExtra(BluffConst.KEY_PLAYERNAME);
		Network.getInstance().setCallback(this);
		SetupTask task = new SetupTask();
		task.execute();
	}

	/** Called when the activity is in the process of closing. */
	protected void onDestroy() {
		super.onDestroy();
		if (mHandler != null) {
			mHandler.removeCallbacks(mTimeoutRunnable);
		}
	}

	@Override
	public void onBackPressed() {
		super.onBackPressed();
		Network.getInstance().disconnect();
	}

	private class SetupTask extends AsyncTask<Void, Void, Void> {

		protected void onPreExecute() {
		}

		protected Void doInBackground(Void... s) {
			Network.getInstance().connect();
			return null;
		}

		protected void onPostExecute(Void result) {

		}
	}

	@Override
	public void onMessageReceived(JSONObject jsonResponse) {
		try {
			if (jsonResponse.getInt(BluffConst.JSON_KEY_ACTION) == BluffConst.JSON_REQ_ACT_LOGIN) {
				if (jsonResponse.getInt(BluffConst.JSON_KEY_RESP) == BluffConst.JSON_VAL_SUCCESS) {

					JSONObject object = new JSONObject();
					object.put(BluffConst.JSON_KEY_ACTION,
							BluffConst.JSON_REQ_ACT_STARTGAME);
					Network.getInstance().sendMessage(object);
				} else if (jsonResponse.getInt(BluffConst.JSON_KEY_RESP) == BluffConst.JSON_VAL_FAIL) {
					Network.getInstance().setCallback(null);
					Network.getInstance().disconnect();

					String _nonUniqueName = getResources().getString(
							R.string.nonunique_player_name_text);
					Toast toast = Toast.makeText(getApplicationContext(),
							_nonUniqueName, Toast.LENGTH_SHORT);
					toast.show();

					finish();
				}

			} else if (jsonResponse.getInt(BluffConst.JSON_KEY_ACTION) == BluffConst.JSON_RESP_ACT_STARTROUND) {
				// success start the game
				Intent pushBluffTable = new Intent(getApplicationContext(),
						BluffTableActivity.class);
				pushBluffTable.putExtra(BluffConst.KEY_PLAYERNAME, name);
				pushBluffTable.putExtra(BluffConst.KEY_SETUPDETAILS,
						jsonResponse.toString());
				startActivity(pushBluffTable);
				finish();
			} else {
				Log.e(Tag,
						"action = "
								+ jsonResponse
										.getInt(BluffConst.JSON_KEY_ACTION));
				Network.getInstance().disconnect();
				finish();
			}

		} catch (JSONException e) {
			e.printStackTrace();
		}

	}

	@Override
	public void connectionUpdate(boolean connected) {
		Integer FAKEHARDCODEDGAMEID = Integer.valueOf(0);
		if (connected) {
			JSONObject object = new JSONObject();
			try {
				object.put(BluffConst.JSON_KEY_ACTION,
						BluffConst.JSON_REQ_ACT_LOGIN);
				object.put(BluffConst.JSON_KEY_NAME, name);
				object.put(BluffConst.JSON_KEY_GAMEID, FAKEHARDCODEDGAMEID);
				Network.getInstance().sendMessage(object);
			} catch (JSONException e) {
				e.printStackTrace();
			}

		} else {
			finish();
		}

	}
}