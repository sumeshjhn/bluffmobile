package com.ece452.bluffmobile.screens;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.graphics.Typeface;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.ece452.bluffmobile.BluffConst;
import com.ece452.bluffmobile.R;
import com.ece452.bluffmobile.customcontrols.UserTurnDialogFragment;
import com.ece452.bluffmobile.customviews.BluffCardView;
import com.ece452.bluffmobile.customviews.BluffTableView;
import com.ece452.bluffmobile.models.BluffCardModel;
import com.ece452.bluffmobile.models.BluffHandModel;
import com.ece452.bluffmobile.models.BluffPlayerModel;
import com.ece452.bluffmobile.network.Network;
import com.ece452.bluffmobile.network.NetworkCallback;

public class BluffTableActivity extends Activity implements NetworkCallback {

	// private Dialog mUserTurnDialog = null;

	private UserTurnDialogFragment mUserTurnDialogFragment = null;
	private BluffTableView mBluffTableView = null;

	private List<BluffPlayerModel> mPlayers = null;
	private List<View> mPlayerViews = null;

	private LinearLayout mDisplayedHandLayout = null;

	private final String TAG = "BluffTableActivity";

	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_bluff_table);

		// get players from intent

		mBluffTableView = (BluffTableView) this
				.findViewById(R.id.viewBluffTableView);
		
		boolean isUserTurn = false;
		try {
			isUserTurn = initializeRound(new JSONObject(this.getIntent()
					.getStringExtra(BluffConst.KEY_SETUPDETAILS)), true);
		} catch (JSONException e) {
			e.printStackTrace();
			finish();
		}
		
		this.mPlayerViews = new ArrayList<View>();
		
		Network.getInstance().setCallback(this);

		this.mDisplayedHandLayout = (LinearLayout) this
				.findViewById(R.id.bottomPlayerLayout);

		// init a view for when user's turn to select hand
		this.mUserTurnDialogFragment = new UserTurnDialogFragment();

		//mBluffTableView.enableCall(false);

		this.setupPlayersOnTable();
		
		try {
			this.highlightCurrentPlayer(
					new JSONObject(this.getIntent().getStringExtra(
							BluffConst.KEY_SETUPDETAILS)).getString(BluffConst.JSON_KEY_PLAYER));
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		if (isUserTurn) {
			mUserTurnDialogFragment.showDialog(getFragmentManager(), true);
		}
	}

	private boolean initializeRound(JSONObject jsonResponse, boolean initializeGame) {
		JSONArray cardsArray = null;
		JSONArray playersArray = null;
		boolean isUserTurn = false;

		try {
			cardsArray = jsonResponse.getJSONArray(BluffConst.JSON_KEY_CARDS);
			playersArray = jsonResponse
					.getJSONArray(BluffConst.JSON_KEY_PLAYERS);
			isUserTurn = jsonResponse.getBoolean(BluffConst.JSON_KEY_USERTURN);
		} catch (JSONException e) {
			e.printStackTrace();
			Network.getInstance().disconnect();
		}

		this.mPlayers = new ArrayList<BluffPlayerModel>();

		Integer _playerIndex = -1;
		
		// set cards for MY player
		for (int i = 0; playersArray != null && i < playersArray.length(); i++) {
			BluffPlayerModel _bPM = new BluffPlayerModel();
			try {
				JSONObject _player = playersArray.getJSONObject(i);
				String playerName = (String) _player
						.get(BluffConst.JSON_KEY_NAME);
				_bPM.setPlayerName(playerName);
				_bPM.setNumCards((Integer) _player
						.get(BluffConst.JSON_KEY_NUMCARDS));
				if (playerName.equals(this.getIntent().getStringExtra(
						BluffConst.KEY_PLAYERNAME))) {
					_playerIndex = i;

					List<BluffCardModel> _listOfCards = new ArrayList<BluffCardModel>();
					for (int j = 0; cardsArray != null
							&& j < cardsArray.length(); j++) {
						BluffCardModel _bcm = new BluffCardModel();
						JSONObject _card = cardsArray.getJSONObject(j);
						_bcm.setCardRank(BluffCardModel.Rank.values()[(Integer) _card
								.get(BluffConst.JSON_KEY_RANK) - 1]);
						_bcm.setCardSuit(BluffCardModel.Suit.values()[(Integer) _card 
								.get(BluffConst.JSON_KEY_SUIT)]);
						_listOfCards.add(_bcm);
					}
					_bPM.setCards(_listOfCards);

				}
			} catch (JSONException e) {
				e.printStackTrace();
				Network.getInstance().disconnect();
			} catch (Exception e) {
				e.printStackTrace();
				Network.getInstance().disconnect();
			}
			mPlayers.add(_bPM);
		}
		
		if (!initializeGame && _playerIndex >= 0) {
			this.updateButtonText();
			this.updateDisplayedHandLayout(_playerIndex);
		}
		mBluffTableView.placeHandClaimOnTable(new BluffHandModel());
		
		return isUserTurn;
	}

	private void highlightCurrentPlayer(String player) {
		for (int i = 0; i < this.mPlayers.size(); i++) {
			if (this.mPlayers.get(i).getPlayerName().equals(player)) {
				PorterDuffColorFilter filter = new PorterDuffColorFilter(
						Color.MAGENTA, PorterDuff.Mode.SRC_ATOP);
				this.mPlayerViews.get(i).getBackground().setColorFilter(filter);
			} else {
				this.mPlayerViews.get(i).invalidateDrawable(
						this.mPlayerViews.get(i).getBackground());
				this.mPlayerViews.get(i).getBackground().clearColorFilter();
			}
		}

	}

	private void setupPlayersOnTable() {
		Button btn = null;
		LinearLayout playerLayout = null;

		for (int i = 0; i < mPlayers.size(); i++) {
			// four sides to the layout: top, right, bottom, left
			switch (i % 3) {
			case 0: {
				playerLayout = (LinearLayout) this
						.findViewById(R.id.topPlayerLayout);
				break;
			}
			case 1: {
				playerLayout = (LinearLayout) this
						.findViewById(R.id.rightPlayerLayout);
				break;
			}
			case 2: {
				playerLayout = (LinearLayout) this
						.findViewById(R.id.leftPlayerLayout);
				break;
			}
			}
			btn = new Button(this);
			// button in sequence represents the player in sequence
			btn.setTag(i);
			btn.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View arg0) {
					updateDisplayedHandLayout(arg0.getTag());
				}
			});

			// need to get player names
			btn.setText(mPlayers.get(i).getPlayerName() + "\n"
					+ mPlayers.get(i).getNumCards());

			this.mPlayerViews.add(btn);

			if (mPlayers
					.get(i)
					.getPlayerName()
					.equals(this.getIntent().getStringExtra(
							BluffConst.KEY_PLAYERNAME))) {
				btn.setTypeface(Typeface
						.create(Typeface.DEFAULT, Typeface.BOLD));
				updateDisplayedHandLayout(btn.getTag());
			}

			playerLayout.addView(btn);
		}

	}
	
	private void updateButtonText() {
		for (int i = 0; i < this.mPlayerViews.size(); i++) {
			((Button) this.mPlayerViews.get(i)).setText(
					this.mPlayers.get(i).getPlayerName() + "\n" + this.mPlayers.get(i).getNumCards());
		}
	}

	private void updateDisplayedHandLayout(Object tag) {
		List<BluffCardModel> _bCMs = null;
		BluffHandModel _bHM = null;
		TextView _playerHandClaim = (TextView) mDisplayedHandLayout
				.findViewById(R.id.playerHandClaimStringRepresentation);
		LinearLayout _cardsLayout = (LinearLayout) mDisplayedHandLayout
				.findViewById(R.id.cardsLayout);

		int size = 0;

		boolean mainPlayer = mPlayers
				.get((Integer) tag)
				.getPlayerName()
				.equals(this.getIntent().getStringExtra(
						BluffConst.KEY_PLAYERNAME));

		if (mainPlayer) {
			_bCMs = mPlayers.get((Integer) (tag)).getCards();
			size = _bCMs.size();
			_playerHandClaim.setVisibility(View.GONE);
			_cardsLayout.setVisibility(View.VISIBLE);
		} else {
			_bHM = mPlayers.get((Integer) (tag)).getHand();
			_playerHandClaim.setVisibility(View.VISIBLE);
			_cardsLayout.setVisibility(View.GONE);
			_playerHandClaim.setText(_bHM.getHandTextRepresentation(
					getResources().getStringArray(R.array.handClaimStrings),
					getResources().getString(R.string.no_hand_claimed_text)));
			size = 0;
		}

		TextView _playerName = (TextView) mDisplayedHandLayout
				.findViewById(R.id.playerName);
		for (int j = 0; j < 5; j++) {

			BluffCardView _bCV = null;
			switch (j) {
			case 0: {
				_bCV = (BluffCardView) mDisplayedHandLayout
						.findViewById(R.id.firstCard);
				break;
			}
			case 1: {
				_bCV = (BluffCardView) mDisplayedHandLayout
						.findViewById(R.id.secondCard);
				break;
			}
			case 2: {
				_bCV = (BluffCardView) mDisplayedHandLayout
						.findViewById(R.id.thirdCard);
				break;
			}
			case 3: {
				_bCV = (BluffCardView) mDisplayedHandLayout
						.findViewById(R.id.fourthCard);
				break;
			}
			case 4: {
				_bCV = (BluffCardView) mDisplayedHandLayout
						.findViewById(R.id.fifthCard);
				break;
			}
			}

			if (j < size) {
				_bCV.setVisibility(View.VISIBLE);
				BluffCardModel.Rank r = BluffCardModel.Rank.EMPTY;
				BluffCardModel.Suit s = BluffCardModel.Suit.EMPTY;
	
				if (mainPlayer) {
					r = _bCMs.get(j).getCardRank();
					s = _bCMs.get(j).getCardSuit();
				} /*
				 * else { r = _bHM.getCardRanks().get(j); s = _bHM.getHandSuit(); }
				 */
				_bCV.setBluffCardRank(r);
				_bCV.setBluffCardSuit(s);
			} else {
				_bCV.setVisibility(View.GONE);
			}
		}
		_playerName.setText(mPlayers.get((Integer) (tag)).getPlayerName() + "\n" 
				+ mPlayers.get((Integer) (tag)).getNumCards());
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		dismissUserTurnDialog();
		Network.getInstance().setCallback(null);
		Network.getInstance().disconnect();
	}

	private void dismissUserTurnDialog() {
		if ((mUserTurnDialogFragment != null)
				&& (mUserTurnDialogFragment.isVisible())) {
			mUserTurnDialogFragment.dismiss();
		}
	}

	@Override
	public void onMessageReceived(JSONObject jsonResponse) {
		try {
			if (jsonResponse.has(BluffConst.JSON_KEY_ACTION)) {
				//mBluffTableView.enableCall(true);

				if (jsonResponse.getInt(BluffConst.JSON_KEY_ACTION) == BluffConst.JSON_RESP_ACT_STARTROUND) {
					boolean isUserTurn = this.initializeRound(jsonResponse, false);
					if (isUserTurn) {
						this.mUserTurnDialogFragment.setMinimumHand(null, null);
						mUserTurnDialogFragment.showDialog(
								getFragmentManager(), true);
					}
				} else if (jsonResponse.getInt(BluffConst.JSON_KEY_ACTION) == BluffConst.JSON_RESP_ACT_BLUFF) {

					BluffHandModel _hand = new BluffHandModel();
					JSONObject _jsonHand = jsonResponse
							.getJSONObject(BluffConst.JSON_KEY_HAND);
					JSONArray _jsonRanks = _jsonHand
							.getJSONArray((BluffConst.JSON_KEY_RANKS));
					List<BluffCardModel.Rank> _ranks = new ArrayList<BluffCardModel.Rank>();
					for (int i = 0; i < _jsonRanks.length(); i++) {
						_ranks.add(i, BluffCardModel.Rank.values()[_jsonRanks
								.getInt(i) - 1]);
					}
					_hand.setCardRanks(_ranks);
					_hand.setHandSuit(BluffCardModel.Suit.values()[_jsonHand
							.getInt(BluffConst.JSON_KEY_SUIT)]);
					_hand.setHandType(BluffHandModel.HandType.values()[_jsonHand
							.getInt(BluffConst.JSON_KEY_HANDTYPE)]);

					for (int i = 0; i < mPlayers.size(); i++) {
						if (mPlayers.get(i).getPlayerName()
								.equals(jsonResponse
										.getString(BluffConst.JSON_KEY_HANDOWNER))) {
							mPlayers.get(i).setHand(_hand);
							break;
						}
					}

					mBluffTableView.placeHandClaimOnTable(_hand);
					Log.d(TAG,
							this.getIntent().getStringExtra(
									BluffConst.KEY_PLAYERNAME)
									+ jsonResponse
											.getString(BluffConst.JSON_KEY_PLAYER));

					if (this.getIntent()
							.getStringExtra(BluffConst.KEY_PLAYERNAME)
							.equals(jsonResponse
									.getString(BluffConst.JSON_KEY_PLAYER))) {
						this.mUserTurnDialogFragment.setMinimumHand(_hand,
								jsonResponse
								.getString(BluffConst.JSON_KEY_HANDOWNER));
						mUserTurnDialogFragment.showDialog(
								getFragmentManager(), false);
					}

					this.highlightCurrentPlayer(jsonResponse
							.getString(BluffConst.JSON_KEY_PLAYER));

				} else if (jsonResponse.getInt(BluffConst.JSON_KEY_ACTION) == BluffConst.JSON_RESP_ACT_CALL) {

					String calledPlayer = jsonResponse
							.getString(BluffConst.JSON_KEY_CALLEDPLAYER);
					String callingPlayer = jsonResponse
							.getString(BluffConst.JSON_KEY_CALLINGPLAYER);
					Boolean isHandPresent = jsonResponse
							.getBoolean(BluffConst.JSON_KEY_ISHANDPRESENT);

					BluffPlayerModel _bPM = null;

					for (int i = 0; i < this.mPlayers.size(); i++) {
						_bPM = this.mPlayers.get(i);
						if (_bPM.getPlayerName().equals(calledPlayer)) {
							break;
						}
					}

					dismissUserTurnDialog();
					mBluffTableView.displayCallOutcome(_bPM, callingPlayer,
							isHandPresent);
				} else if (jsonResponse.getInt(BluffConst.JSON_KEY_ACTION) == BluffConst.JSON_RESP_ACT_ENDGAME) {
					String _winningPlayer = jsonResponse
							.getString(BluffConst.JSON_KEY_WINNER);

					if (_winningPlayer.equals(this.getIntent().getStringExtra(
							BluffConst.KEY_PLAYERNAME))) {
						_winningPlayer = "You";
					} else {
						_winningPlayer = jsonResponse
								.getString(BluffConst.JSON_KEY_WINNER);
					}

					Toast _winnerToast = Toast.makeText(
							this,
							_winningPlayer
									+ " " + getResources().getString(
											R.string.winner_message_text),
							Toast.LENGTH_SHORT);
					_winnerToast.show();
					finish();
				}
			}
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Override
	public void connectionUpdate(boolean connected) {
		Log.d(TAG, (connected) ? " connected " : " connection dropped ");
		if (!connected) {
			finish();
		}
	}
}