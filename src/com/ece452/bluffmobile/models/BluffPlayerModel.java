package com.ece452.bluffmobile.models;

import java.util.ArrayList;
import java.util.List;

public class BluffPlayerModel {

	private List<BluffCardModel> mCards = new ArrayList<BluffCardModel>();
	private BluffHandModel mHand = new BluffHandModel();
	private String mPlayerName = null;
	private int mNumCards = 0;
	
	public BluffHandModel getHand() {
		return mHand;
	}

	public void setHand(BluffHandModel mHand) {
		this.mHand = mHand;
	}

	public List<BluffCardModel> getCards() {
		return mCards;
	}

	public void setCards(List<BluffCardModel> mCards) {
		this.mCards = mCards;
	}
	
	public int getNumCards() {
		return mNumCards;
	}

	public void setNumCards(int mNumCards) {
		this.mNumCards = mNumCards;
	}

	public String getPlayerName() {
		return mPlayerName;
	}

	public void setPlayerName(String mPlayerName) {
		this.mPlayerName = mPlayerName;
	}

}
