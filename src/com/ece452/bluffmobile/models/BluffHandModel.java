package com.ece452.bluffmobile.models;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;

import android.content.Context;

import com.ece452.bluffmobile.BluffConst;
import com.ece452.bluffmobile.R;

public class BluffHandModel {

	public enum HandType {
		EMPTY(0), SINGLE(1), PAIR(2), TWOPAIR(3), TRIPLE(4), STRAIGHT(5), FLUSH(
				6), FULLHOUSE(7), QUAD(8), STRAIGHTFLUSH(9);

		private final int mId;

		private HandType(int id) {
			this.mId = id;
		}

		public int getHandInt() {
			return mId;
		}
	}

	private List<BluffCardModel.Rank> mCardRanks = new ArrayList<BluffCardModel.Rank>();
	private BluffCardModel.Suit mHandSuit = BluffCardModel.Suit.EMPTY;
	private HandType mHandType = BluffHandModel.HandType.EMPTY;

	public List<BluffCardModel.Rank> getCardRanks() {
		if (mCardRanks.isEmpty()) {
			for (int i = 0; i < BluffConst.INITIAL_NUM_OF_CARDS; i++) {
				mCardRanks.add(BluffCardModel.Rank.EMPTY);
			}
		}
		return mCardRanks;
	}

	public void setCardRanks(List<BluffCardModel.Rank> cardRanks) {
		this.mCardRanks = cardRanks;
	}

	/**
	 * 
	 * @param location : use Integer.MAX_VALUE for adding it to the end
	 * @param card
	 */
	public void addCardRank(BluffCardModel.Rank card) {

		if (this.getCardRanks().get(0) == BluffCardModel.Rank.EMPTY) {
			this.mCardRanks.clear();
		}

		if (this.mCardRanks.size() <= BluffConst.INITIAL_NUM_OF_CARDS) {
				this.mCardRanks.add(card);
		}
	}

	public BluffCardModel.Suit getHandSuit() {
		return mHandSuit;
	}

	public void setHandSuit(BluffCardModel.Suit handSuit) {
		this.mHandSuit = handSuit;
	}

	public BluffHandModel.HandType getHandType() {
		return mHandType;
	}

	public void setHandType(HandType handType) {
		this.mHandType = handType;
	}

	public static BluffHandModel.HandType convertToHandTypeEnum(
			String spinnerValue) {
		BluffHandModel.HandType _handType = BluffHandModel.HandType.EMPTY;

		if (spinnerValue.equals("SINGLE")) {
			_handType = BluffHandModel.HandType.SINGLE;
		} else if (spinnerValue.equals("PAIR")) {
			_handType = BluffHandModel.HandType.PAIR;
		} else if (spinnerValue.equals("TWOPAIR")) {
			_handType = BluffHandModel.HandType.TWOPAIR;
		} else if (spinnerValue.equals("TRIPLE")) {
			_handType = BluffHandModel.HandType.TRIPLE;
		} else if (spinnerValue.equals("STRAIGHT")) {
			_handType = BluffHandModel.HandType.STRAIGHT;
		} else if (spinnerValue.equals("FLUSH")) {
			_handType = BluffHandModel.HandType.FLUSH;
		} else if (spinnerValue.equals("FULLHOUSE")) {
			_handType = BluffHandModel.HandType.FULLHOUSE;
		} else if (spinnerValue.equals("QUAD")) {
			_handType = BluffHandModel.HandType.QUAD;
		} else if (spinnerValue.equals("STRAIGHT FLUSH")) {
			_handType = BluffHandModel.HandType.STRAIGHTFLUSH;
		}

		return _handType;
	}
	
	public String getHandTextRepresentation(String[] arrayOfClaimStrings, String noClaimString) {
		String _formatMessage = "";
		List<Object> _arguments = new ArrayList<Object>();

		for (int i = 0; i < this.getCardRanks().size(); i++) {
			_arguments.add(this.getCardRanks().get(i));
		}

		switch (this.getHandType()) {
		case FLUSH: {
			_formatMessage = arrayOfClaimStrings[5];
			_arguments.add(this.getHandSuit().name());
			break;
		}
		case FULLHOUSE: {
			_formatMessage = arrayOfClaimStrings[6];
			break;
		}
		case PAIR: {
			_formatMessage = arrayOfClaimStrings[1];
			break;
		}
		case QUAD: {
			_formatMessage = arrayOfClaimStrings[7];
			break;
		}
		case SINGLE: {
			_formatMessage = arrayOfClaimStrings[0];
			break;
		}
		case STRAIGHT: {
			_formatMessage = arrayOfClaimStrings[4];
			break;
		}
		case STRAIGHTFLUSH: {
			_formatMessage = arrayOfClaimStrings[8];
			_arguments.add(this.getHandSuit().name());
			break;
		}
		case TRIPLE: {
			_formatMessage = arrayOfClaimStrings[3];
			break;
		}
		case TWOPAIR: {
			_formatMessage = arrayOfClaimStrings[2];
			break;
		}
		case EMPTY: {
			_formatMessage = noClaimString;
			break;
		}
		default:
			break;
		}

		MessageFormat _fmt = new MessageFormat(_formatMessage);
		return _fmt.format(_arguments.toArray());
	}
}
