package com.ece452.bluffmobile.models;

public class BluffCardModel {

	public enum Suit{
		EMPTY(0),
		DIAMONDS(1),
		CLUBS(2),
		HEARTS(3),
		SPADES(4);
		
		private final int mId;
		
		private Suit (int id) {
			this.mId=id;
		}
		
		public int getSuitInt() {
			return mId;
		}
	}
	
	public enum Rank{
		EMPTY(0),
		TWO(2),
		THREE(3),
		FOUR(4),
		FIVE(5),
		SIX(6),
		SEVEN(7),
		EIGHT(8),
		NINE(9),
		TEN(10),
		JACK(11),
		QUEEN(12),
		KING(13),
		ACE(14);
		
		private final int mId;
		
		private Rank (int id) {
			this.mId=id;
		}
		
		public int getRankInt() {
			return mId;
		}
	}
	
	private Rank mCardRank = Rank.EMPTY;
	private Suit mCardSuit = Suit.EMPTY;
	
	public Rank getCardRank() {
		return mCardRank;
	}
	public void setCardRank(Rank cardValue) {
		this.mCardRank = cardValue;
	}
	public Suit getCardSuit() {
		return mCardSuit;
	}
	public void setCardSuit(Suit cardSuit) {
		this.mCardSuit = cardSuit;
	}
	
	public static BluffCardModel.Rank convertToCardRankEnum(String spinnerValue) {

		BluffCardModel.Rank _rank = BluffCardModel.Rank.EMPTY;

		if (spinnerValue.equals("TWO")) {
			_rank = BluffCardModel.Rank.TWO;
		} else if (spinnerValue.equals("THREE")) {
			_rank = BluffCardModel.Rank.THREE;
		} else if (spinnerValue.equals("FOUR")) {
			_rank = BluffCardModel.Rank.FOUR;
		} else if (spinnerValue.equals("FIVE")) {
			_rank = BluffCardModel.Rank.FIVE;
		} else if (spinnerValue.equals("SIX")) {
			_rank = BluffCardModel.Rank.SIX;
		} else if (spinnerValue.equals("SEVEN")) {
			_rank = BluffCardModel.Rank.SEVEN;
		} else if (spinnerValue.equals("EIGHT")) {
			_rank = BluffCardModel.Rank.EIGHT;
		} else if (spinnerValue.equals("NINE")) {
			_rank = BluffCardModel.Rank.NINE;
		} else if (spinnerValue.equals("TEN")) {
			_rank = BluffCardModel.Rank.TEN;
		} else if (spinnerValue.equals("JACK")) {
			_rank = BluffCardModel.Rank.JACK;
		} else if (spinnerValue.equals("QUEEN")) {
			_rank = BluffCardModel.Rank.QUEEN;
		} else if (spinnerValue.equals("KING")) {
			_rank = BluffCardModel.Rank.KING;
		} else if (spinnerValue.equals("ACE")) {
			_rank = BluffCardModel.Rank.ACE;
		}

		return _rank;
	}

	public static BluffCardModel.Suit convertToCardSuitEnum(String spinnerValue) {
		BluffCardModel.Suit _suit = BluffCardModel.Suit.EMPTY;

		if (spinnerValue.equals("CLUBS")) {
			_suit = BluffCardModel.Suit.CLUBS;
		} else if (spinnerValue.equals("HEARTS")) {
			_suit = BluffCardModel.Suit.HEARTS;
		} else if (spinnerValue.equals("DIAMONDS")) {
			_suit = BluffCardModel.Suit.DIAMONDS;
		} else if (spinnerValue.equals("SPADES")) {
			_suit = BluffCardModel.Suit.SPADES;
		}

		return _suit;
	}
	
}
